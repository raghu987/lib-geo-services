package in.co.teni.lib.geodistance.model;

import java.io.Serializable;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.FieldDefaults;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
public class GeodistanceResponse implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -6749203934702169199L;

	Integer distanceInMeters;
	
    Integer durationInsecs;
}
