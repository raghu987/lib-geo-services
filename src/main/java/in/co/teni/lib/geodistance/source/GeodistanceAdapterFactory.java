package in.co.teni.lib.geodistance.source;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.NoSuchBeanDefinitionException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Configuration;
import org.springframework.util.StringUtils;

import in.co.teni.lib.geodistance.adapters.google.GoogleAdapter;
import in.co.teni.lib.geodistance.model.Provider;
import lombok.extern.slf4j.Slf4j;

@Configuration
@Slf4j
public class GeodistanceAdapterFactory {

	@Autowired
	ApplicationContext context;

	@PostConstruct
	public void afterPropertiesSet() throws Exception {
		log.info("Initializing GeodistanceAdapter Factory..");
	}

	public GeodistanceAdapter getProviderByName(String providerName) throws Exception {
		Provider provider;
		GeodistanceAdapter geodistanceAdapter;
		if (StringUtils.isEmpty(providerName)) {
			throw new IllegalArgumentException("Empty provider field");
		}
		try {
			log.info("Provider name :" + providerName);
			provider = Provider.valueOf(providerName);
		} catch (Exception e) {
			throw new IllegalArgumentException("Provider " + providerName + " not supported");
		}
		switch (provider) {
		case GOOGLE:
			try {
				geodistanceAdapter = context.getBean(GoogleAdapter.class);
			} catch (NoSuchBeanDefinitionException nsde) {
				log.info("No Bean found of Google, hence creating and registering a new bean");
				geodistanceAdapter = new GoogleAdapter();
			}
			break;

		default:
			throw new IllegalArgumentException("Unable to map provider for " + providerName);
		}
		return geodistanceAdapter;
	}
}
