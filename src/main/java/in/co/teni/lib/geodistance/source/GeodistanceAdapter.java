package in.co.teni.lib.geodistance.source;

import org.springframework.beans.factory.annotation.Autowired;

import in.co.teni.lib.commons.service.RequestValidator;
import in.co.teni.lib.geodistance.interfaces.IGeodistanceService;
import in.co.teni.lib.geodistance.model.GeodistanceRequest;

public abstract class GeodistanceAdapter implements IGeodistanceService {
	
	@Autowired
	RequestValidator<GeodistanceRequest> requestValidator;

	public void validateRequest(GeodistanceRequest request) {
		requestValidator.validate(request);
	}
}
