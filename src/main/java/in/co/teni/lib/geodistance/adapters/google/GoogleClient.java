package in.co.teni.lib.geodistance.adapters.google;

import java.io.IOException;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import in.co.teni.lib.commons.model.ExternalResponse;
import in.co.teni.lib.geodistance.model.GeodistanceRequest;
import in.co.teni.lib.geodistance.model.GeodistanceResponse;
import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class GoogleClient {

	@Value("${geopify.api.key:AIzaSyA4TmWTh2fM3eoLtnHu4dkqDXfjtMO25cM}")
	private String apiKey;

	@Value("${google.api.base.url:https://maps.googleapis.com/maps/api/distancematrix/json}")
	private String googleBaseUrl;

	public ExternalResponse<GeodistanceResponse> getGeodistance(GeodistanceRequest geodistanceRequest) {
		ExternalResponse<GeodistanceResponse> response = null;
		try {
			String uri = buildRequestUri(geodistanceRequest);
			log.info("Fetching Geodistance via Google");
			log.info("uri: {}", uri);

			RestTemplate restTemplate = new RestTemplate();
			ResponseEntity<String> callResponse = restTemplate.getForEntity(uri, String.class);
			log.info("Google Geodistance response: {}", callResponse.toString());

			response = buildResponse(callResponse.getBody());

		} catch (Exception e) {
			String message = "Exception occured while processing Google Geodistance request";
			response = ExternalResponse.errorResponse(null, message, null);
			log.error(message, e);
		}
		return response;
	}

	private ExternalResponse<GeodistanceResponse> buildResponse(String responseBody) throws IOException {
		ExternalResponse<GeodistanceResponse> response;
		GoogleStatusResponse statusResponse = new ObjectMapper().readValue(responseBody, GoogleStatusResponse.class);
		if (statusResponse.getStatus().equals("OK")) {
			response = parseResponse(responseBody);
		} else {
			String responseId = null;
			String errorCode = statusResponse.getStatus();
			String message = statusResponse.getErrorMessage();
			response = ExternalResponse.errorResponse(responseId, message, errorCode);
		}
		return response;
	}

	private String buildRequestUri(GeodistanceRequest geodistanceRequest) {
		return UriComponentsBuilder.fromHttpUrl(googleBaseUrl)
				.queryParam("origins", geodistanceRequest.getOrigin().toString())
				.queryParam("destinations", geodistanceRequest.getDestination().toString())
				.queryParam("key", apiKey).toUriString();
	}

	private ExternalResponse<GeodistanceResponse> parseResponse(String googleResponseBody) throws IOException {
		ExternalResponse<GeodistanceResponse> response = null;
		ObjectMapper mapper = new ObjectMapper();
		JsonNode rootNode, rowsListNode, rowNode, elementsListNode, elementNode;
		GeodistanceResponse geodistanceResponse = null;
		String responseId = null;

		rootNode = mapper.readTree(googleResponseBody);
		rowsListNode = rootNode.get("rows");
		rowNode = rowsListNode.get(0);
		elementsListNode = rowNode.get("elements");
		elementNode = elementsListNode.get(0);

		String status = elementNode.get("status").asText();
		if (status.equals("OK")) {
			String reponseStr = elementNode.toString();
			GoogleGeodistanceResponse googleResponse = new ObjectMapper().readValue(reponseStr, GoogleGeodistanceResponse.class);
			Integer distanceInMeters = googleResponse.getDistance().getValue();
			Integer durationInsecs = googleResponse.getDuration().getValue();
			geodistanceResponse = GeodistanceResponse.builder().distanceInMeters(distanceInMeters).durationInsecs(durationInsecs).build();
			response = ExternalResponse.successResponse(geodistanceResponse, responseId);
		} else {
			response = ExternalResponse.errorResponse(null, null, status);
		}

		return response;
	}

}