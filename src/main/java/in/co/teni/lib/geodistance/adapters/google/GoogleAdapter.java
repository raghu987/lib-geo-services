package in.co.teni.lib.geodistance.adapters.google;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import in.co.teni.lib.commons.model.ExternalResponse;
import in.co.teni.lib.commons.model.Status;
import in.co.teni.lib.geodistance.model.GeodistanceRequest;
import in.co.teni.lib.geodistance.model.GeodistanceResponse;
import in.co.teni.lib.geodistance.source.GeodistanceAdapter;
import in.co.teni.lib.geodistance.source.GeodistanceCacheService;

@Service
public class GoogleAdapter extends GeodistanceAdapter {

	@Autowired
	private GoogleClient googleClient;
	
	@Autowired
	private GeodistanceCacheService geodistanceCacheService;

	@Value("${geodistance.cache.enable:true}")
	Boolean enableGeodistanceCaching;

	@Override
	public ExternalResponse<GeodistanceResponse> getGeodistance(@Valid GeodistanceRequest geodistanceRequest) {
		validateRequest(geodistanceRequest);
		ExternalResponse<GeodistanceResponse> response = googleClient.getGeodistance(geodistanceRequest);
		if (response.getStatus().equals(Status.SUCCESS) && enableGeodistanceCaching) {
			geodistanceCacheService.storeIntoCache(geodistanceRequest.getOrigin(), geodistanceRequest.getDestination(),
					response.getResult());
		}
		return response;
	}

}