package in.co.teni.lib.geodistance.source;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.locationtech.spatial4j.io.GeohashUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import in.co.teni.lib.commons.cache.ICacheProvider;
import in.co.teni.lib.commons.cache.ICacheProvider.CacheBucket;
import in.co.teni.lib.commons.model.Location;
import in.co.teni.lib.geodistance.model.GeodistanceResponse;

@Service
public class GeodistanceCacheService {

	@Autowired
	@Qualifier("redisGeoServicesLib")
	ICacheProvider cacheProvider;

	private static int precision = 7;

	public void storeIntoCache(Location origin, Location destination, GeodistanceResponse geodistanceData) {
		String key = buildKey(origin, destination);
		Map<String, String> dataMap = new HashMap<>(1);
		try {
			String value = new ObjectMapper().writeValueAsString(geodistanceData);
			dataMap.put(key, value);
		} catch (JsonProcessingException e) {
			e.printStackTrace();
		}
		cacheProvider.store(CacheBucket.GEODISTANCE, dataMap);
	}

	public GeodistanceResponse getFromCache(Location origin, Location destination) {
		String key = buildKey(origin, destination);
		String geodistanceResponseStr = cacheProvider.get(CacheBucket.GEODISTANCE, key);
		try {
			if (!StringUtils.isEmpty(geodistanceResponseStr)) {
				return new ObjectMapper().readValue(geodistanceResponseStr, GeodistanceResponse.class);
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}

	private String buildKey(Location origin, Location destination) {
		String originGeoHash = GeohashUtils.encodeLatLon(origin.getLatitude(), origin.getLongitude(), precision);
		String destinationGeoHash = GeohashUtils.encodeLatLon(destination.getLatitude(), destination.getLongitude(), precision);
		return originGeoHash + "," + destinationGeoHash;
	}
}
