package in.co.teni.lib.geodistance.interfaces;

import in.co.teni.lib.commons.model.ExternalResponse;
import in.co.teni.lib.geodistance.model.GeodistanceRequest;
import in.co.teni.lib.geodistance.model.GeodistanceResponse;

public interface IGeodistanceService {

	ExternalResponse<GeodistanceResponse> getGeodistance(GeodistanceRequest geodistanceRequest);
}
