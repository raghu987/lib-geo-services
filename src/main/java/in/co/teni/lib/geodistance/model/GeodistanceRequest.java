package in.co.teni.lib.geodistance.model;

import javax.validation.constraints.NotNull;

import in.co.teni.lib.commons.model.Location;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.FieldDefaults;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
public class GeodistanceRequest {

	@NotNull
    Location origin;
	
	@NotNull
    Location destination;
}
