package in.co.teni.lib.isodistance.source;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.NoSuchBeanDefinitionException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Configuration;
import org.springframework.util.StringUtils;

import in.co.teni.lib.isodistance.adapters.geopify.GeopifyAdapter;
import in.co.teni.lib.isodistance.model.Provider;
import lombok.extern.slf4j.Slf4j;

@Configuration
@Slf4j
public class IsodistanceAdapterFactory {

	@Autowired
	ApplicationContext context;

	@PostConstruct
	public void afterPropertiesSet() throws Exception {
		log.info("Initializing IsodistanceAdapter Factory..");
	}

	public IsodistanceAdapter getProviderByName(String providerName) throws Exception {
		Provider provider;
		IsodistanceAdapter isodistanceAdapter;
		if (StringUtils.isEmpty(providerName)) {
			throw new IllegalArgumentException("Empty provider field");
		}
		try {
			log.info("Provider name :" + providerName);
			provider = Provider.valueOf(providerName);
		} catch (Exception e) {
			throw new IllegalArgumentException("Provider " + providerName + " not supported");
		}
		switch (provider) {
		case GEOPIFY:
			try {
				isodistanceAdapter = context.getBean(GeopifyAdapter.class);
			} catch (NoSuchBeanDefinitionException nsde) {
				log.info("No Bean found of Geopify, hence creating and registering a new bean");
				isodistanceAdapter = new GeopifyAdapter();
			}
			break;

		default:
			throw new IllegalArgumentException("Unable to map provider for " + providerName);
		}

		return isodistanceAdapter;

	}
}
