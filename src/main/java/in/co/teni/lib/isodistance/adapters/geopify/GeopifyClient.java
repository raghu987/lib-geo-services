package in.co.teni.lib.isodistance.adapters.geopify;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import in.co.teni.lib.commons.model.ExternalResponse;
import in.co.teni.lib.isodistance.model.IsodistanceRequest;
import in.co.teni.lib.isodistance.model.IsodistanceResponse;
import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class GeopifyClient {

	@Value("${geopify.api.key:4f4b33aeaa2f4aef8f15ece14b764d95}")
	private String apiKey;

	@Value("${geopify.api.base.url:https://api.geoapify.com/v1/isoline}")
	private String geopifyBaseUrl;

	public ExternalResponse<IsodistanceResponse> getIsodistance(IsodistanceRequest isodistanceRequest) {
		ExternalResponse<IsodistanceResponse> response = null;
		try {
			String uri = buildRequestUri(isodistanceRequest);
			log.info("Fetching Isodistance via Geopify");
			log.info("uri: {}", uri);

			RestTemplate restTemplate = new RestTemplate();
			ResponseEntity<String> callResponse = restTemplate.getForEntity(uri, String.class);
			response = parseResponse(callResponse.getBody());
		} catch (HttpClientErrorException e) {
			response = buildErrorResponse(e);
		} catch (Exception e) {
			String message = "Exception occured while processing Geopify Isodistance request";
			log.error(message, e);
			String responseId = null;
			String errorCode = null;
			response = ExternalResponse.errorResponse(responseId, message, errorCode);
		}
		return response;
	}

	private ExternalResponse<IsodistanceResponse> buildErrorResponse(HttpClientErrorException e) {
		ExternalResponse<IsodistanceResponse> response = null;
		String responseId = null;
		GeopifyErrorResponse errorResponse = null;
		try {
			errorResponse = new ObjectMapper().readValue(e.getResponseBodyAsString(), GeopifyErrorResponse.class);
			String message = errorResponse.getMessage();
			String errorCode = String.valueOf(errorResponse.getStatusCode()) + " " + errorResponse.getError();
			response = ExternalResponse.errorResponse(responseId, message, errorCode);
		} catch (IOException error) {
			log.error("Error occured while parsing geopify error response", error);
		}
		return response;
	}

	private String buildRequestUri(IsodistanceRequest isodistanceRequest) {
		return UriComponentsBuilder.fromHttpUrl(geopifyBaseUrl).queryParam("type", "distance")
				.queryParam("mode", "drive")
				.queryParam("range", isodistanceRequest.getDistanceRangeInMeters())
				.queryParam("lat", isodistanceRequest.getLocation().getLatitude())
				.queryParam("lon", isodistanceRequest.getLocation().getLongitude()).queryParam("apiKey", apiKey)
				.toUriString();
	}

	private ExternalResponse<IsodistanceResponse> parseResponse(String geopifyResponseBody) throws IOException {
		ObjectMapper mapper = new ObjectMapper();
		JsonNode rootNode, featuresListNode, featureNode, geometryNode, multipolygonNode, isodistanceMultiPolygonNode,
				isodistanceOuterPolygonNode, coordinate;
		Double longitude, latitude;
		IsodistanceResponse isodistanceResponse = null;
		String responseId = null;

		/*
		 * Extract the outermost Isodistance polygon
		 */
		rootNode = mapper.readTree(geopifyResponseBody);
		featuresListNode = rootNode.get("features");
		featureNode = featuresListNode.get(0);
		geometryNode = featureNode.get("geometry");
		multipolygonNode = geometryNode.get("coordinates");
		isodistanceMultiPolygonNode = multipolygonNode.get(0);
		isodistanceOuterPolygonNode = isodistanceMultiPolygonNode.get(0);
		Iterator<JsonNode> elements = isodistanceOuterPolygonNode.iterator();
		List<List<Double>> locationsList = new ArrayList<>();

		int index = -1;
		while (elements.hasNext()) {
			List<Double> location = new ArrayList<>(2);
			coordinate = elements.next();
			longitude = coordinate.get(0).asDouble();
			latitude = coordinate.get(1).asDouble();
			
			// Skips consecutive duplicate co-ordinates
			if (!CollectionUtils.isEmpty(locationsList)) {
				if (locationsList.get(index).get(0).compareTo(longitude) == 0
						&& locationsList.get(index).get(1).compareTo(latitude) == 0) {
					log.info("Skipping duplicate co-ordinate in Isodistance Polygon");
					continue;
				}
			}
			location.add(longitude);
			location.add(latitude);
			locationsList.add(location);
			++index;
		}
		isodistanceResponse = IsodistanceResponse.builder().isodistancePolygon(locationsList).build();
		return ExternalResponse.successResponse(isodistanceResponse, responseId);
	}

}