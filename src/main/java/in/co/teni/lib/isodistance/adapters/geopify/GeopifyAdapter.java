package in.co.teni.lib.isodistance.adapters.geopify;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import in.co.teni.lib.commons.model.ExternalResponse;
import in.co.teni.lib.commons.model.Status;
import in.co.teni.lib.isodistance.model.IsodistanceRequest;
import in.co.teni.lib.isodistance.model.IsodistanceResponse;
import in.co.teni.lib.isodistance.source.IsodistanceAdapter;
import in.co.teni.lib.isodistance.source.IsodistanceCacheService;

@Service
public class GeopifyAdapter extends IsodistanceAdapter {

	@Autowired
	private GeopifyClient geopifyClient;

	@Autowired
	private IsodistanceCacheService isodistanceCacheService;

	@Value("${isodistance.cache.enable:true}")
	Boolean enableIsodistanceCaching;

	@Override
	public ExternalResponse<IsodistanceResponse> getIsodistance(IsodistanceRequest isodistanceRequest) {
		validateRequest(isodistanceRequest);
		ExternalResponse<IsodistanceResponse> response = geopifyClient.getIsodistance(isodistanceRequest);
		if (response.getStatus().equals(Status.SUCCESS) && enableIsodistanceCaching) {
			isodistanceCacheService.storeIntoCache(isodistanceRequest.getLocation(), response.getResult());
		}
		return response;
	}

}