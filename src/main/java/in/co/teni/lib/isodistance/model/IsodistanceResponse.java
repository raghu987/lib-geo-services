package in.co.teni.lib.isodistance.model;

import java.io.Serializable;
import java.util.List;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.FieldDefaults;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
public class IsodistanceResponse implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -3477828687156574867L;
	/*
	 * Example: [ [longitude, latitude], .. ] which is a standard GeoJson format to represent a polygon
	 */	
	List<List<Double>> isodistancePolygon;

}
