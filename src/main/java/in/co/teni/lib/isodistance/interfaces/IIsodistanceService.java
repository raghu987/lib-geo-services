package in.co.teni.lib.isodistance.interfaces;

import in.co.teni.lib.commons.model.ExternalResponse;
import in.co.teni.lib.isodistance.model.IsodistanceRequest;
import in.co.teni.lib.isodistance.model.IsodistanceResponse;

public interface IIsodistanceService {

	ExternalResponse<IsodistanceResponse> getIsodistance(IsodistanceRequest isodistanceRequest);
}
