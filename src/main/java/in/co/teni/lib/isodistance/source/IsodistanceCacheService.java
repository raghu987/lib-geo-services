package in.co.teni.lib.isodistance.source;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.locationtech.spatial4j.io.GeohashUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import in.co.teni.lib.commons.cache.ICacheProvider;
import in.co.teni.lib.commons.cache.ICacheProvider.CacheBucket;
import in.co.teni.lib.commons.model.Location;
import in.co.teni.lib.isodistance.model.IsodistanceResponse;

@Service
public class IsodistanceCacheService {

	@Autowired
	@Qualifier("redisGeoServicesLib")
	ICacheProvider cacheProvider;

	private static int precision = 7;

	public void storeIntoCache(Location location, IsodistanceResponse isodistanceData) {
		String geoHash = GeohashUtils.encodeLatLon(location.getLatitude(), location.getLongitude(), precision);
		Map<String, String> dataMap = new HashMap<>(1);
		try {
			dataMap.put(geoHash, new ObjectMapper().writeValueAsString(isodistanceData));
		} catch (JsonProcessingException e) {
			e.printStackTrace();
		}
		cacheProvider.store(CacheBucket.ISODISTANCE, dataMap);
	}

	public IsodistanceResponse getFromCache(Location location) {
		String geoHash = GeohashUtils.encodeLatLon(location.getLatitude(), location.getLongitude(), precision);
		String isodistanceResponseStr = cacheProvider.get(CacheBucket.ISODISTANCE, geoHash);
		try {
			if (!StringUtils.isEmpty(isodistanceResponseStr)) {
				return new ObjectMapper().readValue(isodistanceResponseStr, IsodistanceResponse.class);
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}
}
