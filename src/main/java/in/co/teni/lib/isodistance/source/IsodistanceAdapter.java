package in.co.teni.lib.isodistance.source;

import org.springframework.beans.factory.annotation.Autowired;

import in.co.teni.lib.commons.service.RequestValidator;
import in.co.teni.lib.isodistance.interfaces.IIsodistanceService;
import in.co.teni.lib.isodistance.model.IsodistanceRequest;

public abstract class IsodistanceAdapter implements IIsodistanceService {

	@Autowired
	RequestValidator<IsodistanceRequest> requestValidator;

	public void validateRequest(IsodistanceRequest request) {
		requestValidator.validate(request);
	}

}
