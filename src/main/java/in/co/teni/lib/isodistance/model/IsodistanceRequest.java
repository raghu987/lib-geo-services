package in.co.teni.lib.isodistance.model;


import javax.validation.constraints.NotNull;

import in.co.teni.lib.commons.model.Location;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.FieldDefaults;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
public class IsodistanceRequest {

	@NotNull
    Location location;
	
	@NotNull
    Long distanceRangeInMeters;
}
