package in.co.teni.lib.commons.cache;

import java.util.Map;

import org.redisson.api.RMap;
import org.redisson.api.RedissonClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import lombok.extern.slf4j.Slf4j;

@Component("redisGeoServicesLib")
@Slf4j
public class RedisCacheProvider implements ICacheProvider {

	@Autowired
	RedissonClient redissonClient;

	/**
	 * @param cacheBucket
	 * @param tuples:     HashMap of key and value which would be stored in redis
	 * 
	 * @return true if tuples are stored successfully otherwise false
	 */
	@Override
	public boolean store(CacheBucket cacheBucket, Map<String, String> tuples) {
		if (tuples.isEmpty()) {
			return true; // returns true when empty
		}
		log.info("Prefix is: {}, Records to be insterted in redis: {}", cacheBucket.name(), tuples);
		try {
			RMap<String, Object> map = redissonClient.getMap(cacheBucket.name().concat("Map"));
			for (Map.Entry<String, String> entry : tuples.entrySet()) {
				map.put(entry.getKey(), entry.getValue());
			}
			return true;
		} catch (Exception e) {
			log.error("Error while storing into cache bucket {}", cacheBucket, e);
			return false;
		}
	}

	@Override
	public String get(CacheBucket cacheBucket, String key) {
		String value = "";
		log.info("Prefix is: {}, Key to be searched in redis: {}", cacheBucket.name(), key);
		try {
			RMap<String, String> map = redissonClient.getMap(cacheBucket.name().concat("Map"));
			value = map.get(key);
		} catch (Exception e) {
			log.error("Error while fetching from cache for key " + key, e);
		}
		return value;
	}
}
