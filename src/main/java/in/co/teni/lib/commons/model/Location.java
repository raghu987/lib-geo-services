package in.co.teni.lib.commons.model;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.FieldDefaults;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
public class Location {

	Double longitude;
	Double latitude;

    /**
     * Converts a given Location Object to latitude,longitude
     * 
     * @param location
     * @return Location as Latitude,longitude
     */
	@Override
	public String toString() {
		return String.valueOf(this.latitude).concat(",").concat(String.valueOf(this.longitude));
	}
}
