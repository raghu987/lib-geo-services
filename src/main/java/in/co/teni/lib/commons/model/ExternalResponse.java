package in.co.teni.lib.commons.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import static in.co.teni.lib.commons.model.Status.SUCCESS;
import static in.co.teni.lib.commons.model.Status.ERROR;
import lombok.Data;

@Data
@JsonInclude(Include.NON_NULL)
public class ExternalResponse<T> {

	private final T result;
	private final String responseId;
	private final Status status;
	private final String message;
	private final String errorCode;

	private ExternalResponse(final T result, final Status status, final String responseId, final String message,
			final String errorCode) {
		this.result = result;
		this.status = status;
		this.message = message;
		this.errorCode = errorCode;
		this.responseId = responseId;
	}

	public static final <T> ExternalResponse<T> successResponse(final T result, final String responseId) {
		return new ExternalResponse<>(result, SUCCESS, responseId, null, null);
	}

	public static final <T> ExternalResponse<T> errorResponse(final String responseId, final String message,
			final String errorCode) {
		return new ExternalResponse<>(null, ERROR, responseId, message, errorCode);
	}

}
