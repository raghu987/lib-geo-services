package in.co.teni.lib.commons.service;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;

import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import org.springframework.web.server.ResponseStatusException;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class RequestValidator<T> {

	public void validate(final T request) {
		Validator validator = getValidator();
		log.info("Validating request, {}", request);

		Set<ConstraintViolation<T>> violations = validator.validate(request);
		if (!CollectionUtils.isEmpty(violations)) {
			String errorResponse = buildErrors(violations);
			log.error("Request validation failed: {}", errorResponse);
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST, errorResponse);
		}
	}

	private String buildErrors(Set<ConstraintViolation<T>> violations) {
		Map<String, String> errors = new HashMap<>(violations.size());
		String errorResponse = null;

		for (ConstraintViolation<T> violation : violations) {
			String parameter = violation.getPropertyPath().toString();
			String error = violation.getMessage();
			errors.put(parameter, error);
		}

		try {
			errorResponse = new ObjectMapper().writeValueAsString(errors);
		} catch (JsonProcessingException e) {
			e.printStackTrace();
		}
		return errorResponse;
	}

	private Validator getValidator() {
		ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
		return factory.getValidator();
	}

}
