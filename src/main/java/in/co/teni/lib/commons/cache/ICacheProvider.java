package in.co.teni.lib.commons.cache;

import java.util.Map;

public interface ICacheProvider {
	public enum CacheBucket {
		ISODISTANCE, GEODISTANCE
	}

	boolean store(CacheBucket cacheBucket, Map<String, String> tuples);

	String get(CacheBucket cacheBucket, String key);
}
