package in.co.teni.lib.test;

import java.util.Objects;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.databind.ObjectMapper;

import in.co.teni.lib.commons.model.ExternalResponse;
import in.co.teni.lib.geodistance.model.GeodistanceRequest;
import in.co.teni.lib.geodistance.model.GeodistanceResponse;
import in.co.teni.lib.geodistance.source.GeodistanceAdapterFactory;
import in.co.teni.lib.geodistance.source.GeodistanceCacheService;
import in.co.teni.lib.isodistance.model.IsodistanceRequest;
import in.co.teni.lib.isodistance.model.IsodistanceResponse;
import in.co.teni.lib.isodistance.source.IsodistanceAdapterFactory;
import in.co.teni.lib.isodistance.source.IsodistanceCacheService;
import lombok.extern.slf4j.Slf4j;

/**
 * Exposes endoints to test the following geo services:
 * 
 * 1. Isodistance 2.Geodistance
 * 
 * @author Raghu
 *
 */
@RestController
@RequestMapping("/test")
@Slf4j
public class GeoServicesTestController {

	@Autowired
	IsodistanceAdapterFactory isodistancefactory;

	@Autowired
	GeodistanceAdapterFactory geodistancefactory;

	@Autowired
	IsodistanceCacheService isodistanceCacheService;

	@Autowired
	GeodistanceCacheService geodistanceCacheService;

	/**
	 * 
	 * @param request
	 * @param provider
	 * @return
	 * @throws Exception
	 */
	@PostMapping("/isodistance")
	ResponseEntity<ExternalResponse<IsodistanceResponse>> getIsodistance(@RequestBody IsodistanceRequest request,
			@RequestParam(value = "provider", required = true) String provider,
			@RequestParam(value = "forceFetch", defaultValue = "false") Boolean forceFetch) throws Exception {

		ExternalResponse<IsodistanceResponse> response = null;
		IsodistanceResponse isodistanceResponse = null;

		log.info("Isodistance request received via test API");
		log.info("Request Params: Provider: {}  forceFetch: {}", provider, forceFetch);
		log.info("Request Body: {}", new ObjectMapper().writeValueAsString(request));

		if (!forceFetch) {
			isodistanceResponse = isodistanceCacheService.getFromCache(request.getLocation());
		}

		if (Objects.nonNull(isodistanceResponse)) {
			log.info("Found isodistance from the cache");
			response = ExternalResponse.successResponse(isodistanceResponse, null);
		} else {
			response = isodistancefactory.getProviderByName(provider).getIsodistance(request);
		}
		return ResponseEntity.ok(response);
	}

	/**
	 * 
	 * @param request
	 * @param provider
	 * @return
	 * @throws Exception
	 */
	@PostMapping("/geodistance")
	ResponseEntity<ExternalResponse<GeodistanceResponse>> getGeodistance(@RequestBody GeodistanceRequest request,
			@RequestParam(value = "provider", required = true) String provider,
			@RequestParam(value = "forceFetch", defaultValue = "false") Boolean forceFetch) throws Exception {

		ExternalResponse<GeodistanceResponse> response = null;
		GeodistanceResponse geodistanceResponse = null;

		log.info("Geodistance request received via test API");
		log.info("Request Params: Provider: {}  forceFetch: {}", provider, forceFetch);
		log.info("Request Body: {}", new ObjectMapper().writeValueAsString(request));

		if (!forceFetch) {
			geodistanceResponse = geodistanceCacheService.getFromCache(request.getOrigin(), request.getDestination());
		}

		if (Objects.nonNull(geodistanceResponse)) {
			log.info("Found geodistance from the cache");
			response = ExternalResponse.successResponse(geodistanceResponse, null);
		} else {
			response = geodistancefactory.getProviderByName(provider).getGeodistance(request);
		}
		return ResponseEntity.ok(response);
	}

}
